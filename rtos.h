#pragma once

// maximum number of functions to have.
// this method might be changed later. TODO
# define MAX_FUNCTION_NUMBER 10

//number of functions scheduled.
int num_of_functions = 0;

// all functions should be compatible with this type: void func()
typedef void (*simple_func_type)(void);

// list of scheduled functions
simple_func_type functionsList[MAX_FUNCTION_NUMBER];

// how often each function should execute? (frequency)
double func_frequency[MAX_FUNCTION_NUMBER];

// how often each function should execute? (time)
double func_period[MAX_FUNCTION_NUMBER];

//when is the next time each function should run
double func_next_run[MAX_FUNCTION_NUMBER];

// template
void voidFunc(void){
    return;
}

// add a function to the schedules list
/*
    Use this to add your functions.
    note that functions have to get nothing and return nothing.
*/
void addFunc(simple_func_type func, double frequency);

// do a step.
/*
    this checks functions execution times and decides to whether run them.
    run this in a timer interrupt or in an infinite loop.
    you must also give system time as an argument to this.
*/
void check(double time);
