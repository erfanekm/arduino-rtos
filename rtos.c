#include "rtos.h"

// add a function to schedule
void addFunc(simple_func_type func, double frequency)
{
    // don't let number exceed the max
    if(num_of_functions >= MAX_FUNCTION_NUMBER)
    {
        /* ERROR */
        return;
    }

    // add function to list
    functionsList[num_of_functions] = func;
    func_frequency[num_of_functions] = frequency;
    // period is used to know execution time instead of frequency.
    func_period[num_of_functions] = 1 / frequency;

    num_of_functions++;   

}

// each step, check time and exxecute functions.
void check(double time)
{
    int i;
    // amongst all the functions,
    for(i = 0; i < num_of_functions; i++)
    {
        // if it's time for this to run,
        if(func_next_run[i] <= time)
        {
            // execute it,
            functionsList[i]();
            // then set next execution time for it.
            func_next_run[i] += func_period[i];
        }
    }
}
